package com.itheima.mp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.mp.domain.po.User;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import org.mybatis.spring.annotation.MapperScan;

import java.util.List;

public interface UserMapper extends BaseMapper<User> {

    void saveUser(User user);


    void deleteUser(Long id);

    void updateUser(User user);

    User queryUserById(@Param("id") Long id);

    List<User> queryUserByIds(@Param("ids") List<Long> ids);
    @Update("UPDATE user SET balance = balance - #{money} WHERE id=#{id}")
    void deductBalance(@Param("id") Long id,@Param("money") Integer money);
}
